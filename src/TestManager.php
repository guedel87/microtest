<?php

  /*
   * The MIT License
   *
   * Copyright 2018 Guillaume de Lestanville <guillaume.delestanville@proximit.fr>.
   *
   * Permission is hereby granted, free of charge, to any person obtaining a copy
   * of this software and associated documentation files (the "Software"), to deal
   * in the Software without restriction, including without limitation the rights
   * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   * copies of the Software, and to permit persons to whom the Software is
   * furnished to do so, subject to the following conditions:
   *
   * The above copyright notice and this permission notice shall be included in
   * all copies or substantial portions of the Software.
   *
   * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   * THE SOFTWARE.
   */

  namespace Guedel\Microtest;

  /**
   * Manage a set of tests
   *
   * @author Guillaume de Lestanville <guillaume.delestanville@proximit.fr>
   */
  class TestManager
  {
    const UNDEFINED_TEST = 'undefined';
    const UNIT_TEST = 'unit test';
    const FUNC_TEST = 'functional test';
    const SCENARIO = 'scenario';

    private $suites = array();

    /**
     * Crée un titre
     * @param type $msg
     */
    public static function title($msg)
    {
      echo NL;
      echo $msg, NL;
      echo str_repeat('=', strlen($msg)), NL;
    }

    /**
     *
     * @param \Guedel\Microtest\UnitTest $test
     * @param type $kind
     */
    public function registerSuite(UnitTest $test, $kind = self::UNDEFINED_TEST)
    {
      if ($kind === null) {
        $kind = self::UNDEFINED_TEST;
      }
      $this->suites[$kind][] = $test;
    }

    /**
     * Run all registered tests
     * @param type $kind
     */
    public function testAll($kind = null)
    {
      if ($kind === null) {
        // Run all registered test
        foreach ($this->suites as $testKind => $collTest) {
          self::title($testKind);
          foreach ($collTest as $test) {
            $test->testAll();
          }
        }
      } else {
        // Run only one kind of test
        self::title($kind);
        if (isset($this->suites[$kind])) {
          foreach ($this->suites[$kind] as $test) {
            $test->testAll();
          }
        }
      }
    }

  }
