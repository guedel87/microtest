<?php
  namespace Guedel\Microtest;

  /**
   * Contains all functions for assertion
   */
  class Assert
  {

    /**
     * Check that two values are the same
     * @param mixed $ref
     * @param mixed $test
     * @param string $message
     * @throws AssertException
     */
    public static function equal($ref, $test, $message = '')
    {
      if ($ref != $test) {
        if (!$message) {
          throw new AssertException("test value are different to $ref");
        } else {
          throw new AssertException($message);
        }
      }
    }

    /**
     * Check that two values are the same
     * @param mixed $ref
     * @param mixed $test
     * @param string $message
     * @throws AssertException
     */
    public static function same($ref, $test, $message = '')
    {
      if ($ref !== $test) {
        if (!$message) {
          throw new AssertException("test value are different to $ref");
        } else {
          throw new AssertException($message);
        }
      }
    }

    /**
     * Check that two values are different
     * @param mixed $ref reference value
     * @param mixed $test tested value
     * @param string $message message if the test fail
     * @throws AssertException
     */
    public static function different($ref, $test, $message = '')
    {
      if ($ref == $test) {
        if (!$message) {
          throw new AssertException('values are equals');
        } else {
          throw new AssertException($message);
        }
      }
    }

    /**
     *
     * @param mixed $bound1
     * @param mixed $bound2
     * @param mixed $test
     * @param string $message
     * @throws AssertException
     */
    public static function between($bound1, $bound2, $test, $message = '')
    {
      if ($test < $bound1 || $test > $bound2) {
        if (!$message) {
          throw new AssertException("not between $bound1 and $bound2 bounds");
        } else {
          throw new AssertException($message);
        }
      }
    }

    /**
     * Test of floating values.
     * @param double $ref
     * @param double $value
     * @param double $epsilon
     * @param string $message
     */
    public static function equivalent($ref, $value, $epsilon = 1e-7, $message = '')
    {
      self::between($ref - $epsilon, $ref + $epsilon, $value, $message);
    }

    /**
     * Check existence of variable
     * @param mixed $item
     * @param type $message message if the test fail
     * @throws AssertException
     */
    public static function isVariableExists($item, $message = '')
    {
      if (isset($item)) {
        if (!$message) {
          throw new AssertException("the element doesn't exist");
        } else {
          throw new AssertException($message);
        }
      }
    }

    /**
     * Check that the result is true
     * @param bool $test value to test
     * @param type $message message if the test fail
     * @throws AssertException
     */
    public static function isTrue($test, $message = '')
    {
      if (!$test) {
        if (!$message) {
          throw new AssertException('the test is negative');
        } else {
          throw new AssertException($message);
        }
      }
    }

    /**
     * Check that the result is false
     * @param bool $test value to test
     * @param string $message message if the test fail
     * @throws AssertException
     */
    public static function isFalse($test, $message = '')
    {
      if ($test) {
        if (!$message) {
          throw new AssertException('the test is positive');
        } else {
          throw new AssertException($message);
        }
      }
    }

    /**
     * Check that an exception was raised
     * @param string $exceptionClass
     * @param callable $fn
     * @param string $message
     * @throws AssertException
     */
    public static function exception($exceptionClass, callable $fn, $message = '')
    {
      $attend = null;
      try {
        call_user_func($fn);
      } catch (\Exception $ex) {
        $attend = $ex;
      }
      if (!is_a($attend, $exceptionClass)) {
        if (!$message) {
          if ($attend === null) {
            throw new AssertException("The exception $exceptionClass does not occur");
          } else {
            throw new AssertException("The catched exception is not of type $exceptionClass");
          }
        } else {
          throw new AssertException($message);
        }
      }
    }

    /**
     * Always fail. Throw an exception with the message
     * @param string $message
     * @throws AssertException
     */
    public static function fail($message)
    {
      throw new AssertException($message);
    }

    /**
     * Always fail. Stop the execution of the sequence of tests
     * @param string $message
     * @throws FatalException
     */
    public static function fatal($message)
    {
      throw new FatalException($message);
    }

  }
