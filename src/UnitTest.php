<?php

  /*
   * Copyright (C) 2016 Proximit
   *
   * @author Guillaume de Lestanville <guillaume.delestanville@proximit.fr>.
   * @since 08/02/2016.
   *
   * Fonctions et classes de base pour les tests unitaires
   */

  namespace Guedel\Microtest;

  use Guedel\Microtest\Report\TestReport;

  if (!defined('NL')) {
    define('NL', PHP_EOL);
  }

  /**
   * Classe de regroupement de tests unitaires
   */
  class UnitTest
  {
    /**
     *
     * @var array tests registered
     */
    protected $testCases = array();

    /**
     *
     * @var string
     */
    protected $unitTitle = '';

    /**
     *
     * @var TestReport
     */
    protected $report = null;

    public function __construct($title, TestReport $report = null)
    {
      if ($report === null) {
        $this->report = new TestReport();
      } else {
        $this->report = $report;
      }
      $this->unitTitle = $title;
    }

    /**
     * set the title
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
      $this->unitTitle = $title;
      return $this;
    }

    /**
     * get the title
     * @return string
     */
    public function getTitle()
    {
      return $this->unitTitle;
    }

    /**
     *
     * @param \Guedel\Microtest\TestReport $report
     * @return $this
     */
    public function setReport(TestReport $report)
    {
      $this->report = $report;
      return $this;
    }

    /**
     * @return \Guedel\Microtest\Report\TestReport
     */
    public function getReport()
    {
      return $this->report;
    }

    /**
     * Crée un titre
     * @param string $msg
     */
    public function title()
    {
      if ($this->unitTitle) {
        $msg = $this->unitTitle;
      } else {
        $msg = __CLASS__;
      }
      if ($this->report === null) {
        echo NL;
        echo $msg, NL;
        echo str_repeat('-', strlen($msg)), NL;
      } else {
        $this->report->beginTestSuite($msg);
      }
    }

    /**
     * Affichage du diagnostic
     * @param bool $ok
     * @param string $cause
     */
    protected function echoDiag($ok, $cause, $location)
    {
      if ($this->report === null) {
        echo $ok ? "OK" : ('FAIL (' . $cause . ')'), NL;
      } else {
        if ($ok) {
          $this->report->successTestCase();
        } else {
          $this->report->failTestCase($cause, $location);
        }
      }
    }

    /**
     *
     * @param int $id
     * @param string $name
     */
    protected function echoPrepare($id, $name)
    {
      if ($this->report === null) {
        echo $id, '- ', $name . ': ';
      } else {
        $this->report->beginTestCase($id, $name);
      }
    }

    /**
     * Ajout d'un test simple
     * @param string $title
     * @param callable $fn fonction de rappel à executer
     */
    public function addTest($title, $fn)
    {
      $this->testCases[$title] = $fn;
    }

    /**
     *
     * @param string $function
     * @param string $desc
     * @param callable $fn
     */
    public function addFunctionTest($function, $desc, $fn)
    {
      // TODO
    }

    /**
     *
     * @param string $classname
     * @param string $methodname
     * @param string $desc
     * @param callabe $fn
     */
    public function addMethodTest($classname, $methodname, $desc, $fn)
    {
      // TODO
    }

    /**
     * Procédure d'exécution de tous les tests ajoutés préalablement
     */
    public function testAll()
    {
        // display only if defined
      $this->title();
      $nb_reussis = 0;
      $count = 0;
      foreach ($this->testCases as $title => $test) {
        try {
          $count ++;
          $ok = true;
          $ret = false;
          $cause = '';
          $location = null;
          $this->echoPrepare($count, $title);
          call_user_func($test);
        } catch (FatalException $ex) {
          $ok = false;
          $cause = $ex->getMessage();
          $location = array($ex->getFile(), $ex->getLine());
          $ret = true;
        } catch (AssertException $ex) {
          $ok = false;
          $cause = '(assertion) ' . $ex->getMessage();
          $location = array($ex->getFile(), $ex->getLine());
        } catch (\Exception $ex) {
          $ok = false;
          $cause = $ex->getMessage();
          $location = array($ex->getFile(), $ex->getLine());
        }
        $this->echoDiag($ok, $cause, $location);
        if ($ok) {
          $nb_reussis++;
        }
        if ($ret) {
          echo 'interruption required', NL;
          break;
        }
      }
      if ($this->report === null) {
        echo NL, 'success: ', $nb_reussis, ' / ', count($this->testCases), NL;
      } else {
        $this->report->endTestSuite();
      }
    }
  }
