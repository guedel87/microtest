<?php

  namespace Guedel\Microtest;

  class AssertException extends \Exception
  {

    public function __construct($message, $code = 0, $previous = null)
    {
      parent::__construct($message, $code, $previous);
    }

  }
