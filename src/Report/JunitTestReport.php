<?php
  /*
   * The MIT License
   *
   * Copyright 2019 Guillaume de Lestanville <guillaume.delestanville@proximit.fr>.
   *
   * Permission is hereby granted, free of charge, to any person obtaining a copy
   * of this software and associated documentation files (the "Software"), to deal
   * in the Software without restriction, including without limitation the rights
   * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   * copies of the Software, and to permit persons to whom the Software is
   * furnished to do so, subject to the following conditions:
   *
   * The above copyright notice and this permission notice shall be included in
   * all copies or substantial portions of the Software.
   *
   * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   * THE SOFTWARE.
   */

  /*
   * Copyright (C) 2019 Proximit
   *
   * @author Guillaume de Lestanville <guillaume.delestanville@proximit.fr>.
   * @since 23/01/2019.
   *
   *
   */

  namespace Guedel\Microtest\Report;

  /**
   * Generate the output to the Junit file format
   *
   * @author Guillaume de Lestanville <guillaume.delestanville@proximit.fr>
   */
  class JunitTestReport extends TestReport
  {
    private $testsuites = array();
    /**
     *
     * @var TestSuite
     */
    private $currentTestsuite = null;
    /**
     *
     * @var TestCase
     */
    private $currentTestCase = null;
    private $filename = null;

    const STATUS = 'status';
    const STATUS_FAIL = 'fail';
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    public function __construct($filename)
    {
      parent::__construct();
      $this->filename = $filename;
    }

    /**
     * introduce a test case
     * @param mixed $id
     * @param string $name
     */
    public function beginTestCase($id, $name)
    {
      parent::beginTestCase($id, $name);
      $this->currentTestCase = new TestCase($id, $name);
    }

    public function successTestCase()
    {
      parent::successTestCase();
      if ($this->currentTestCase !== null) {
        $this->currentTestCase->status = self::STATUS_SUCCESS;
      }
      $this->commitTestCase();
    }

    public function failTestCase($cause, $location = null)
    {
      parent::failTestCase($cause, $location);
      if ($this->currentTestCase !== null) {
        $this->currentTestCase->status = self::STATUS_FAIL;
        $this->currentTestCase->cause = $cause;
        $this->currentTestCase->location = $location;
      }
      $this->commitTestCase();
    }

    /**
     * Ajoute le cas de test à la suite
     */
    private function commitTestCase()
    {
      if ($this->currentTestCase !== null) {
        $this->currentTestsuite->cases[] = $this->currentTestCase;
      }
      $this->currentTestCase = null;
    }

    public function beginTestSuite($name)
    {
      parent::beginTestSuite($name);
      $this->currentTestsuite = new TestSuite($name);
    }

    public function endTestSuite()
    {
      parent::endTestSuite();
      if ($this->currentTestsuite !== null) {
        $this->testsuites[] = $this->currentTestsuite;
      }
      $this->currentTestsuite = null;
    }

    /**
     * Do an export to the junit xml file
     * @param string $filename
     */
    private function exportToJunit($filename)
    {
      $xml = new \XMLWriter();
      $xml->openMemory();
      $xml->setIndent(true);
      $xml->startDocument('1.0');
      // $xml->startDocument('1.0', 'UTF-8');
      $xml->startElement('testsuites');

      foreach ($this->testsuites as $suite) {
        $xml->startElement('testsuite');
        $xml->writeAttribute('tests', count($suite->cases));
        $failureCount = 0;
        foreach ($suite->cases as $case) {
          $xml->startElement('testcase');
          $xml->writeAttribute('id', $case->id);
          $xml->writeAttribute('name', $case->name);
          //$xml->writeAttribute('classname', $suite['classname']);
          if ($case->status === self::STATUS_FAIL) {
            $xml->startElement('failure');
            $xml->writeAttribute('message', $case->cause);
            $xml->endElement();
            $failureCount++;
          }

          $xml->endElement();
        }
        $xml->writeAttribute('failures', $failureCount);
        $xml->endElement();
      }

      $xml->endElement();
      $xml->endDocument();
      file_put_contents($filename, $xml->outputMemory());
      unset($xml);
    }

    public function finalize()
    {
      $this->exportToJunit($this->filename);
    }

  }
