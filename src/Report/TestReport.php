<?php
  /*
   * The MIT License
   *
   * Copyright 2019 Guillaume de Lestanville <guillaume.delestanville@proximit.fr>.
   *
   * Permission is hereby granted, free of charge, to any person obtaining a copy
   * of this software and associated documentation files (the "Software"), to deal
   * in the Software without restriction, including without limitation the rights
   * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   * copies of the Software, and to permit persons to whom the Software is
   * furnished to do so, subject to the following conditions:
   *
   * The above copyright notice and this permission notice shall be included in
   * all copies or substantial portions of the Software.
   *
   * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   * THE SOFTWARE.
   */

  /*
   * @author Guillaume de Lestanville <guillaume.delestanville@proximit.fr>.
   * @since 23/01/2019.
   */

  namespace Guedel\Microtest\Report;

  /**
   * Manage the report of tests
   *
   * @author Guillaume de Lestanville <guillaume.delestanville@proximit.fr>
   */
  if (!defined('NL')) {
    define('NL', PHP_EOL);
  }

  class TestReport
  {
    private $totalHit = 0;
    private $totalSuccess = 0;

    public function __construct()
    {
    }

    public function beginTestSuite($name)
    {
      echo NL;
      echo $name, NL;
      echo str_repeat('-', strlen($name)), NL;
      $this->totalHit = 0;
      $this->totalSuccess = 0;
    }

    public function endTestSuite()
    {
      echo NL, 'success: ', $this->totalSuccess, ' / ', $this->totalHit, NL;
    }

    public function beginTestCase($id, $name)
    {
      echo $id, '- ', $name . ': ';
      $this->totalHit++;
    }

    public function failTestCase($cause, $location = null)
    {
      if ($location === null) {
        $loc = '';
      } else {
        if (is_array($location)) {
          $loc = implode(':', $location) . ' :';
        } else {
          $loc = $location . ' :';
        }
      }
      echo 'FAIL (' . $loc . $cause . ')', NL;
    }

    public function successTestCase()
    {
      echo 'OK', NL;
      $this->totalSuccess ++;
    }

    public function finalize()
    {
    }
  }
