<?php

  namespace Guedel\Microtest;

  /**
   * Indique une exception fatale et interrompt la suite des tests
   */
  class FatalException extends \Exception
  {

    public function __construct($message, $code = 0, $previous = null)
    {
      parent::__construct($message, $code, $previous);
    }

  }
