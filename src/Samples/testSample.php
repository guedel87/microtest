<?php

  /*
   * Copyright (C) 2016 Proximit
   *
   * @author Guillaume de Lestanville <guillaume.delestanville@proximit.fr>.
   * @since 26/08/2016.
   *
   * Sample test file
   *
   * You can run it with the following command:
   * % php testSample.php
   */

  namespace Guedel\Microtest\Samples;

// Do some initializations
  include_once "bootstrap.php";

  use \Guedel\Microtest\UnitTest;
  use \Guedel\Microtest\Assert;

// Init the test session
  $ut = new UnitTest('sample unit test');

// first test
  $ut->addTest('succeded test', function () {
    Assert::isTrue(true, "this must be true");
  });

// other test run in sequence
  $ut->addTest('failed test', function () {
    Assert::isFalse(true, "this must be false");
  });

  $ut->addTest('exception test', function () {
    Assert::exception('\Exception', function () {
      throw new \Exception('sample exception');
    });
  });

  $ut->addTest('fatal test', function () {
    Assert::fatal("fatal test.");
  });

  $ut->addTest('not run test', function () {
    // this should not be run
  });

  // Run all defined tests
  $ut->testAll();
