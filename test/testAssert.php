<?php
  require_once 'bootstrap.php';

  use Guedel\Microtest\Assert;
  use Guedel\Microtest\AssertException;

  $ut = new guedel\Microtest\UnitTest('Test de la classe Assert', $report);

  $ut->addTest('equal with message', function () {
    try {
      Assert::equal(1, 1, '1 = 1');
      Assert::equal(1, 2, '1 = 1');
    } catch (\Exception $ex) {
      if (! $ex instanceof AssertException) {
        Assert::fail();
      }
    }
  });

  $ut->addTest('equal without message', function () {
    try {
      Assert::equal(1, 1);
      Assert::equal(1, 2);
    } catch (\Exception $ex) {
      if (! $ex instanceof AssertException) {
        Assert::fail();
      }
    }
  });

  $ut->testAll();