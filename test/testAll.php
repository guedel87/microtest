<?php
  require_once 'bootstrap.php';

  $report = new \Guedel\Microtest\Report\TestReport();
  $rep = getopt('x', array('xml'));
  if (count($rep)) {
    if (isset($rep['x']) || isset($rep['xml'])) {
      $report = new guedel\Microtest\Report\JunitTestReport('microtest.xml');
    }
  }

  include 'testAssert.php';

  $report->finalize();

