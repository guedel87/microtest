Presentation
==
Microtest is a simple test framework. You can use it for unit tests and
functional test.

It requires PHP 5.4 minimal just for use of lambda functions and exceptions.

Why I wrote Microtest ?
--
For my old php projects, I don't have a test framework and PhpUnit is 
too complex and not compatible with imperative approach.

I need a simple function to run a set of tests very simple to write.

And now I use Microtest in some projets as black box to test the new functions
than I write.


How to install ?
==

With Composer
--
Go on the root of your own project. And now type:

    composer install --dev guedel/microtest

Other methods
--
TODO with Git or Unzip

How to use ?
== 
Just look at folder src/Sampples. I've created a sample test. There's no 
obligation to create a bootstrap and an autoload files.

Run test
--
With your shell cd in your test folder. 

In windows command shell:

    cd src\Samples
    
In linux shell:

    cd src/Samples
    
And now run your test with php line commad:

    php testSample.php

And you have an outpout like this:

    sample unit test
    ----------------
    1- succeded test: OK
    2- failed test: FAIL ((assertion) this must be false)
    3- exception test: OK
    4- fatal test: FAIL (fatal test.)
    interruption required

    success: 2 / 5
    
If you want to run a set of of test I suggest to create a PHP script who 
includes all test scripts you want.

Example:

    <?php
    // File: testAll.php
    require 'testSample.php'
    require 'testOne.php'
    require 'testTwo.php'

Creating test
--
This is not an objet approach. You create some functions (named or lambda)
And you register it using the UnitTest::add_test method.

Into the function use the differents assertion methods provided by the UnitTest
class

You need to:
- include UnitTest and Assert classes using the `use` instruction
- add a title to the script with UnitTest::title() method
- instanciate a UnitTest object : $ut = new UnitTest()
- add somme test function with UnitTest::add_test() method and add some 
  assertion tests.
- run the test with the UnitTest::test_all() method.

That's all

Pre and post test
--
In the global section of your script you can add functions, classes and 
instances. You can access it using the global statement or the magic var
$_GLOBAL.

Example:

    $myInt = 10;
    $ut = new UnitTest();
    
    $ut->add_test('Int test', function() {
        global $myInt;
        Assert::is_equal(10, $myInt);
        
        // Do some changes
        $myInt = 'ten';
    });

    $ut->add_test('myInt is integer ?', function() {
        global $myInt;
        Assert::is_true(is_int($myInt));
        
    });
    
    $ut->test_all();
    
You can do it. All tests are run in order of their registration.